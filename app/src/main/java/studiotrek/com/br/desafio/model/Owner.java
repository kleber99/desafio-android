package studiotrek.com.br.desafio.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Kleber on 20/06/2017.
 */

public class Owner implements Parcelable {
    private String login;
    private String avatar_url;

    public Owner() {
        super();
    }

    public Owner(Parcel in) {
        super();
        readFromParcel(in);
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(login);
        dest.writeString(avatar_url);
    }

    private void readFromParcel(Parcel in) {
        this.login = in.readString();
        this.avatar_url = in.readString();
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Owner> CREATOR = new Parcelable.Creator<Owner>() {
        @Override
        public Owner createFromParcel(Parcel in) {
            return new Owner(in);
        }

        @Override
        public Owner[] newArray(int size) {
            return new Owner[size];
        }
    };

}
