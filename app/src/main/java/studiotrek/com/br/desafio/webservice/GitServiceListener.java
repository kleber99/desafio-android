package studiotrek.com.br.desafio.webservice;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import studiotrek.com.br.desafio.util.Constante;

/**
 * Created by Kleber on 19/06/2017.
 */

class ResponseGitServiceListener implements Response.Listener<JSONObject> {

    private Context context;

    public ResponseGitServiceListener(Context context) {
        this.context = context;
    }

    @Override
    public void onResponse(JSONObject response) {
        Intent intent = new Intent(Constante.RESPOSTA_GIT_SERVICE);

        Bundle bundle = new Bundle();

        try {
            bundle.putString(Constante.BUNDLE_GIT_SUCESS, response.getJSONArray("items").toString());
            intent.putExtras(bundle);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        context.sendBroadcast(intent);
    }
}

class ErrorGitServiceListener implements Response.ErrorListener {

    private Context context;

    public ErrorGitServiceListener(Context context) {
        this.context = context;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Intent intent = new Intent(Constante.RESPOSTA_GIT_SERVICE);

        Bundle bundle = new Bundle();
        bundle.putString(Constante.BUNDLE_GIT_ERROR, error.toString());
        intent.putExtras(bundle);

        context.sendBroadcast(intent);
    }
}
