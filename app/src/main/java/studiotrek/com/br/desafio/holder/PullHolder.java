package studiotrek.com.br.desafio.holder;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;
import studiotrek.com.br.desafio.R;
import studiotrek.com.br.desafio.model.Pull;
import studiotrek.com.br.desafio.util.Util;

/**
 * Created by Kleber on 22/06/2017.
 */

public class PullHolder extends RecyclerView.ViewHolder {

    private Context context;
    private CircleImageView ivPullFoto;
    private TextView tvPullUsuario;
    private TextView tvPullTitulo;
    private TextView tvPullBody;
    private TextView tvPullData;
    private Pull pull;

    public PullHolder(final Context context, final View itemView) {
        super(itemView);

        this.context = context;
        this.ivPullFoto = (CircleImageView) itemView.findViewById(R.id.iv_pull_foto);
        this.tvPullUsuario = (TextView) itemView.findViewById(R.id.tv_pull_usuario);
        this.tvPullTitulo = (TextView) itemView.findViewById(R.id.tv_pull_titulo);
        this.tvPullBody = (TextView) itemView.findViewById(R.id.tv_pull_body);
        this.tvPullData = (TextView) itemView.findViewById(R.id.tv_pull_data);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(pull.getHtml_url()));
                context.startActivity(intent);
            }
        });

    }

    public void setElements(Pull pull) {
        this.pull = pull;

        Picasso.with(context)
                .load(pull.getUser().getAvatar_url())
                .into(ivPullFoto);

        tvPullUsuario.setText(pull.getUser().getLogin());
        tvPullTitulo.setText(pull.getTitle());
        tvPullBody.setText(pull.getBody());

        tvPullData.setText(Util.parseDateString(pull.getUpdated_at()));
    }

}
