package studiotrek.com.br.desafio.util;

import android.util.Log;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import studiotrek.com.br.desafio.model.Pull;
import studiotrek.com.br.desafio.model.Repositorio;

/**
 * Created by Kleber on 20/06/2017.
 */

public class Util {

    public static List<Repositorio> parseRepositorio(String json) {
        List<Repositorio> repositorios = null;

        try {
            Repositorio[] array = new Gson().fromJson(json, Repositorio[].class);
            repositorios = Arrays.asList(array);
        } catch (Exception e) {
            Log.e("erro", e.getMessage());
        }

        return repositorios;
    }

    public static ArrayList<Pull> parsePull(String json) {
        ArrayList<Pull> pulls = null;

        try {
            Pull[] array = new Gson().fromJson(json, Pull[].class);
            pulls = new ArrayList<>(Arrays.asList(array));
        } catch (Exception e) {
            Log.e("erro", e.getMessage());
        }

        return pulls;
    }

    public static String parseDateString(String data) {
        String retorno = "";

        try {

            if(!"".equals(data)) {
                SimpleDateFormat simpleDateBack = new SimpleDateFormat("yyyy-MM-dd");
                Calendar calendar = Calendar.getInstance();

                calendar.setTime(simpleDateBack.parse(data));
                SimpleDateFormat simpleDateFront = new SimpleDateFormat("dd/MM/yyyy");

                if (calendar != null) {
                    retorno = simpleDateFront.format(calendar.getTime());
                }
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return retorno;
    }

}


