package studiotrek.com.br.desafio.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Kleber on 20/06/2017.
 */

public class User implements Parcelable {

    private String login;
    private String avatar_url;

    public User() {
        super();
    }

    public User(Parcel in) {
        super();
        readFromParcel(in);
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(login);
        dest.writeString(avatar_url);
    }

    private void readFromParcel(Parcel in) {
        login = in.readString();
        avatar_url = in.readString();
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
