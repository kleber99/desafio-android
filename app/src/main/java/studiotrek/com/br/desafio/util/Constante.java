package studiotrek.com.br.desafio.util;

/**
 * Created by Kleber on 19/06/2017.
 */

public interface Constante {
    String BUNDLE_GIT_SUCESS = "bundleGitSucess";
    String BUNDLE_GIT_ERROR = "bundleGitError";
    String BUNDLE_REPOSITORIO = "bundleRepositorio";

    String BUNDLE_PULL_SUCESS = "bundleGitSucess";
    String BUNDLE_PULL_ERROR = "bundleGitError";

    String RESPOSTA_GIT_SERVICE = "respostaGitService";
    String RESPOSTA_PULL_SERVICE = "respostaPullService";
    String URL_GIT_REPOSITORIOS = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1";

    String MSG_ERROR = "error";
}
