package studiotrek.com.br.desafio.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import studiotrek.com.br.desafio.R;
import studiotrek.com.br.desafio.holder.RepositorioHolder;
import studiotrek.com.br.desafio.model.Repositorio;

/**
 * Created by Kleber on 21/06/2017.
 */

public class RepositorioAdapter extends RecyclerView.Adapter<RepositorioHolder> {

    private Context context;
    private List<Repositorio> repositorios;

    public RepositorioAdapter(Context context, List<Repositorio> repositorios) {
        this.context = context;
        this.repositorios = repositorios;
    }

    @Override
    public RepositorioHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_repositorio, parent, false);
        RepositorioHolder viewHolder = new RepositorioHolder(context, view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RepositorioHolder holder, int position) {
        Repositorio repositorio = repositorios.get(position);
        holder.setElements(repositorio);
    }

    public List<Repositorio> getItems() {
        return repositorios;
    }

    @Override
    public int getItemCount() {
        return repositorios.size();
    }
}
