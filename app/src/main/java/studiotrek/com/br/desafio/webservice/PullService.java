package studiotrek.com.br.desafio.webservice;

import android.content.Context;
import android.os.AsyncTask;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import studiotrek.com.br.desafio.util.Constante;

/**
 * Created by Kleber on 20/06/2017.
 */

public class PullService extends AsyncTask<String, Void, Void> {

    private Context context;

    public PullService(Context context) {
        this.context = context;
    }

    @Override
    protected Void doInBackground(String... params) {

        RequestQueue requestQueue;
        Cache cache = new DiskBasedCache(context.getCacheDir(), 1024 * 1024);
        Network network = new BasicNetwork(new HurlStack());

        requestQueue = new RequestQueue(cache, network);
        requestQueue.start();

        String criador = params[0];
        String repositorio = params[1];

        String url = "https://api.github.com/repos/" + criador + "/" + repositorio + "/pulls";

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null,
                new ResponsePullServiceListener(context), new ErrorPullServiceListener(context));

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        requestQueue.add(request);

        return null;
    }
}
