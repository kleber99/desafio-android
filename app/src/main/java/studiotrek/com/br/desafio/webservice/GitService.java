package studiotrek.com.br.desafio.webservice;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;

import studiotrek.com.br.desafio.util.Constante;

/**
 * Created by Kleber on 19/06/2017.
 */

public class GitService extends AsyncTask<String, Void, Void> {

    private Context context;
    private ProgressBar progressBar;

    public GitService(Context context, ProgressBar progressBar) {
        this.context = context;
        this.progressBar = progressBar;
    }

    @Override
    protected void onPreExecute() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected Void doInBackground(String... params) {
        RequestQueue requestQueue;
        Cache cache = new DiskBasedCache(context.getCacheDir(), 1024 * 1024);
        Network network = new BasicNetwork(new HurlStack());

        requestQueue = new RequestQueue(cache, network);
        requestQueue.start();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, Constante.URL_GIT_REPOSITORIOS,
                null, new ResponseGitServiceListener(context), new ErrorGitServiceListener(context));

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        requestQueue.add(request);

        return null;
    }

    @Override
    protected void onPostExecute(Void param) {
        progressBar.setVisibility(View.GONE);
    }

}
