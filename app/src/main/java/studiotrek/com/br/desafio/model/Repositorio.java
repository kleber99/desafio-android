package studiotrek.com.br.desafio.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Kleber on 20/06/2017.
 */

public class Repositorio implements Parcelable {
    private String name;
    private String description;
    private Integer stargazers_count;
    private Integer forks;
    private Owner owner;

    public Repositorio() {
        super();
    }

    public Repositorio(Parcel in) {
        super();
        readFromParcel(in);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStargazers_count() {
        return stargazers_count;
    }

    public void setStargazers_count(Integer stargazers_count) {
        this.stargazers_count = stargazers_count;
    }

    public Integer getForks() {
        return forks;
    }

    public void setForks(Integer forks) {
        this.forks = forks;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(description);
        if (stargazers_count == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(stargazers_count);
        }
        if (forks == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(forks);
        }
        dest.writeValue(owner);
    }

    private void readFromParcel(Parcel in) {
        name = in.readString();
        description = in.readString();
        stargazers_count = in.readByte() == 0x00 ? null : in.readInt();
        forks = in.readByte() == 0x00 ? null : in.readInt();
        owner = (Owner) in.readValue(Owner.class.getClassLoader());
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Repositorio> CREATOR = new Parcelable.Creator<Repositorio>() {
        @Override
        public Repositorio createFromParcel(Parcel in) {
            return new Repositorio(in);
        }

        @Override
        public Repositorio[] newArray(int size) {
            return new Repositorio[size];
        }
    };
}
