package studiotrek.com.br.desafio.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.github.pwittchen.infinitescroll.library.InfiniteScrollListener;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import studiotrek.com.br.desafio.R;
import studiotrek.com.br.desafio.adapter.PullAdapter;
import studiotrek.com.br.desafio.adapter.RepositorioAdapter;
import studiotrek.com.br.desafio.model.Pull;
import studiotrek.com.br.desafio.model.Repositorio;
import studiotrek.com.br.desafio.util.Constante;
import studiotrek.com.br.desafio.util.Util;
import studiotrek.com.br.desafio.webservice.GitService;
import studiotrek.com.br.desafio.webservice.PullService;

/**
 * Created by Kleber on 22/06/2017.
 */

public class PullActivity extends AppCompatActivity {

    private RecyclerView rvPull;
    private LinearLayoutManager layoutManager;
    private ArrayList<Pull> pulls;
    private Repositorio repositorio;

    private int page;
    private static final int MAX_ITEMS_PER_REQUEST = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {

            if(bundle.getParcelable(Constante.BUNDLE_REPOSITORIO) != null) {
                repositorio = bundle.getParcelable(Constante.BUNDLE_REPOSITORIO);
            }

        }

        rvPull = (RecyclerView) findViewById(R.id.rv_pull);
        callPullService();
    }

    private InfiniteScrollListener createInfiniteScrollListener() {
        return new InfiniteScrollListener(MAX_ITEMS_PER_REQUEST, layoutManager) {
            @Override public void onScrolledToEnd(final int firstVisibleItemPosition) {

                int start = ++page * MAX_ITEMS_PER_REQUEST;
                final boolean allItemsLoaded = start >= pulls.size();
                if (!allItemsLoaded) {
                    int end = start + MAX_ITEMS_PER_REQUEST;
                    final List<Pull> itemsLocal = getItemsToBeLoaded(start, end);

                    refreshView(rvPull, new PullAdapter(getApplicationContext(), itemsLocal), firstVisibleItemPosition);
                }
            }
        };
    }

    private List<Pull> getItemsToBeLoaded(int start, int end) {
        List<Pull> newItems = pulls.subList(start, end);
        final List<Pull> oldItems = ((PullAdapter) rvPull.getAdapter()).getItems();

        final List<Pull> itemsLocal = new LinkedList<>();
        itemsLocal.addAll(oldItems);
        itemsLocal.addAll(newItems);
        return itemsLocal;
    }

    private void carregarList(List<Pull> pulls) {
        PullAdapter adapter = null;

        rvPull.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvPull.setLayoutManager(layoutManager);


        if(pulls.size() > MAX_ITEMS_PER_REQUEST) {
            adapter = new PullAdapter(this, pulls.subList(page, MAX_ITEMS_PER_REQUEST));
        } else {
            adapter = new PullAdapter(this, pulls);
        }

        rvPull.setAdapter(adapter);

        rvPull.addOnScrollListener(createInfiniteScrollListener());
    }

    public void callPullService() {
        try {
            BroadcastReceiver receiverGit;
            IntentFilter intentFilter = new IntentFilter(Constante.RESPOSTA_PULL_SERVICE);

            new PullService(this).execute(repositorio.getOwner().getLogin(), repositorio.getName());

            receiverGit = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    Bundle bundle = intent.getExtras();
                    if (bundle != null) {

                        if(bundle.getParcelableArrayList(Constante.BUNDLE_PULL_SUCESS) != null) {
                            pulls = bundle.getParcelableArrayList(Constante.BUNDLE_PULL_SUCESS);
                            carregarList(pulls);
                        }

                        if (bundle.getString(Constante.BUNDLE_PULL_ERROR) != null) {
                            Toast.makeText(context, "Problema de conexão", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            };

            registerReceiver(receiverGit, intentFilter);
        } catch (Exception e) {
            Log.e(Constante.MSG_ERROR, e.getMessage());
        }

    }

}
