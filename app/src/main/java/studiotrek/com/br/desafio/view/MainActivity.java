package studiotrek.com.br.desafio.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.github.pwittchen.infinitescroll.library.InfiniteScrollListener;

import java.util.LinkedList;
import java.util.List;

import studiotrek.com.br.desafio.R;
import studiotrek.com.br.desafio.adapter.RepositorioAdapter;
import studiotrek.com.br.desafio.model.Repositorio;
import studiotrek.com.br.desafio.util.Constante;
import studiotrek.com.br.desafio.util.Util;
import studiotrek.com.br.desafio.webservice.GitService;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rvRepositorio;
    private LinearLayoutManager layoutManager;
    private ProgressBar progressBar;
    private List<Repositorio> repositorios;

    private int page;
    private static final int MAX_ITEMS_PER_REQUEST = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        rvRepositorio = (RecyclerView) findViewById(R.id.rv_repositorio);
        callGitService();
    }

    private InfiniteScrollListener createInfiniteScrollListener() {
        return new InfiniteScrollListener(MAX_ITEMS_PER_REQUEST, layoutManager) {
            @Override public void onScrolledToEnd(final int firstVisibleItemPosition) {

                int start = ++page * MAX_ITEMS_PER_REQUEST;
                final boolean allItemsLoaded = start >= repositorios.size();
                if (allItemsLoaded) {
                    progressBar.setVisibility(View.GONE);
                } else {
                    int end = start + MAX_ITEMS_PER_REQUEST;
                    final List<Repositorio> itemsLocal = getItemsToBeLoaded(start, end);

                    refreshView(rvRepositorio, new RepositorioAdapter(getApplicationContext(), itemsLocal), firstVisibleItemPosition);
                }


            }
        };
    }

    private List<Repositorio> getItemsToBeLoaded(int start, int end) {
        List<Repositorio> newItems = repositorios.subList(start, end);

        final List<Repositorio> oldItems = ((RepositorioAdapter) rvRepositorio.getAdapter()).getItems();

        final List<Repositorio> itemsLocal = new LinkedList<>();
        itemsLocal.addAll(oldItems);
        itemsLocal.addAll(newItems);
        return itemsLocal;
    }

    private void carregarList() {
        rvRepositorio.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvRepositorio.setLayoutManager(layoutManager);

        RepositorioAdapter adapter = new RepositorioAdapter(this, repositorios.subList(page, MAX_ITEMS_PER_REQUEST));
        rvRepositorio.setAdapter(adapter);

        rvRepositorio.addOnScrollListener(createInfiniteScrollListener());
    }

    public void callGitService() {
        try {
            BroadcastReceiver receiverGit;
            IntentFilter intentFilter = new IntentFilter(Constante.RESPOSTA_GIT_SERVICE);

            new GitService(this, progressBar).execute();

            receiverGit = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    Bundle bundle = intent.getExtras();
                    if (bundle != null) {
                        if (bundle.getString(Constante.BUNDLE_GIT_SUCESS) != null) {
                            repositorios = Util.parseRepositorio(bundle.getString(Constante.BUNDLE_GIT_SUCESS));
                            carregarList();
                        }
                    }
                }
            };

            registerReceiver(receiverGit, intentFilter);
        } catch (Exception e) {
            Log.e(Constante.MSG_ERROR, e.getMessage());
        }

    }

}
