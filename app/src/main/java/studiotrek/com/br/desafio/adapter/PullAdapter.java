package studiotrek.com.br.desafio.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import studiotrek.com.br.desafio.R;
import studiotrek.com.br.desafio.holder.PullHolder;
import studiotrek.com.br.desafio.holder.RepositorioHolder;
import studiotrek.com.br.desafio.model.Pull;
import studiotrek.com.br.desafio.model.Repositorio;

/**
 * Created by Kleber on 22/06/2017.
 */

public class PullAdapter extends RecyclerView.Adapter<PullHolder> {

    private Context context;
    private List<Pull> pulls;

    public PullAdapter(Context context, List<Pull> pulls) {
        this.context = context;
        this.pulls = pulls;
    }

    @Override
    public PullHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_pull, parent, false);
        PullHolder viewHolder = new PullHolder(context, view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PullHolder holder, int position) {
        Pull pull = pulls.get(position);
        holder.setElements(pull);
    }

    public List<Pull> getItems() {
        return pulls;
    }

    @Override
    public int getItemCount() {
        return pulls.size();
    }

}
