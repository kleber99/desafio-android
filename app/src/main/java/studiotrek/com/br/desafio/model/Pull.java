package studiotrek.com.br.desafio.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Kleber on 20/06/2017.
 */

public class Pull implements Parcelable {

    private String title;
    private String body;
    private String updated_at;
    private String html_url;
    private User user;

    public Pull() {
        super();
    }

    public Pull(Parcel in) {
        super();
        readFromParcel(in);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getHtml_url() {
        return html_url;
    }

    public void setHtml_url(String html_url) {
        this.html_url = html_url;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(body);
        dest.writeString(updated_at);
        dest.writeString(html_url);
        dest.writeValue(user);
    }

    private void readFromParcel(Parcel in) {
        title = in.readString();
        body = in.readString();
        updated_at = in.readString();
        html_url = in.readString();
        user = (User) in.readValue(User.class.getClassLoader());
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Pull> CREATOR = new Parcelable.Creator<Pull>() {
        @Override
        public Pull createFromParcel(Parcel in) {
            return new Pull(in);
        }

        @Override
        public Pull[] newArray(int size) {
            return new Pull[size];
        }
    };
}