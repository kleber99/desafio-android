package studiotrek.com.br.desafio.webservice;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.JsonArray;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import studiotrek.com.br.desafio.model.Pull;
import studiotrek.com.br.desafio.util.Constante;
import studiotrek.com.br.desafio.util.Util;

/**
 * Created by Kleber on 20/06/2017.
 */

class ResponsePullServiceListener implements Response.Listener<JSONArray> {

    private Context context;

    public ResponsePullServiceListener(Context context) {
        this.context = context;
    }

    @Override
    public void onResponse(JSONArray response) {
        Intent intent = new Intent(Constante.RESPOSTA_PULL_SERVICE);

        Bundle bundle = new Bundle();
        ArrayList<Pull> pulls = Util.parsePull(response.toString());

        bundle.putParcelableArrayList(Constante.BUNDLE_PULL_SUCESS, pulls);

        intent.putExtras(bundle);

        context.sendBroadcast(intent);
    }
}

class ErrorPullServiceListener implements Response.ErrorListener {

    private Context context;

    public ErrorPullServiceListener(Context context) {
        this.context = context;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Intent intent = new Intent(Constante.RESPOSTA_PULL_SERVICE);

        Bundle bundle = new Bundle();
        bundle.putString(Constante.BUNDLE_PULL_ERROR, error.toString());
        intent.putExtras(bundle);

        context.sendBroadcast(intent);
    }
}