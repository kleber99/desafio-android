package studiotrek.com.br.desafio.holder;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.BundleCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import studiotrek.com.br.desafio.R;
import studiotrek.com.br.desafio.model.Repositorio;
import studiotrek.com.br.desafio.util.Constante;
import studiotrek.com.br.desafio.view.PullActivity;

/**
 * Created by Kleber on 21/06/2017.
 */

public class RepositorioHolder extends RecyclerView.ViewHolder {

    private Context context;
    private CircleImageView ivRepositorioFoto;
    private TextView tvRepositorioNome;
    private TextView tvRepositorioDescricao;
    private TextView tvRepositorioUsuario;
    private TextView tvRepositorioStar;
    private TextView tvRepositorioFork;
    private Repositorio repositorio;

    public RepositorioHolder(final Context context, final View itemView) {
        super(itemView);

        this.context = context;
        this.ivRepositorioFoto = (CircleImageView) itemView.findViewById(R.id.iv_repositorio_foto);
        this.tvRepositorioNome = (TextView) itemView.findViewById(R.id.tv_repositorio_nome);
        this.tvRepositorioDescricao = (TextView) itemView.findViewById(R.id.tv_repositorio_descricao);
        this.tvRepositorioUsuario = (TextView) itemView.findViewById(R.id.tv_repositorio_usuario);
        this.tvRepositorioStar = (TextView) itemView.findViewById(R.id.tv_repositorio_star);
        this.tvRepositorioFork = (TextView) itemView.findViewById(R.id.tv_repositorio_fork);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PullActivity.class);

                Bundle bundle = new Bundle();
                bundle.putParcelable(Constante.BUNDLE_REPOSITORIO, repositorio);
                intent.putExtras(bundle);

                context.startActivity(intent);
            }
        });

    }

    public void setElements(Repositorio repositorio) {
        this.repositorio = repositorio;

        Picasso.with(context)
                .load(repositorio.getOwner().getAvatar_url())
                .into(ivRepositorioFoto);

        tvRepositorioNome.setText(repositorio.getName());
        tvRepositorioDescricao.setText(repositorio.getDescription());
        tvRepositorioUsuario.setText(repositorio.getOwner().getLogin());
        tvRepositorioStar.setText(String.valueOf(repositorio.getStargazers_count()));
        tvRepositorioFork.setText(String.valueOf(repositorio.getForks()));
    }

}
